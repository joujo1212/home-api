const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const logger = require('morgan');
const cookieParser = require('cookie-parser');

const authMiddleware = require('../routes/middleware');
const mongoService = require('../services/mongoService');
const users = require('../routes/users');

export default class Rest {
  private app = express();

  constructor() {
    this.init();
  }

  private init() {
    // view engine setup
    this.app.set('views', path.join(__dirname, '../views'));
    this.app.set('view engine', 'pug');
    this.app.use(logger('dev'));

    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({extended: false}));
    this.app.use(cookieParser());

    this.app.use(express.static(path.join(__dirname, 'public')));

    this.app.use('/users', users);

    // route middleware to verify a token
    this.app.use(authMiddleware);

    // catch 404 and forward to error handler
    this.app.use((req, res, next) => {
      const err = new Error('Not Found');
      (<any>err).status = 404;
      next(err);
    });

    // error handler
    this.app.use((err, req, res, next) => {
      // set locals, only providing error in development
      res.locals.message = err.message;
      res.locals.error = req.app.get('env') === 'development' ? err : {};

      // render the error page
      res.status(err.status || 500);
      res.render('error');
    });

    mongoService.connectOnce(function (db) {
      console.log('DB connected to: ', db.topology.s.host);
    });

  }

  public start() {
    this.app.listen(3000, function () {
      console.log('Home-api is running on port 3000');
    });
  }
}
