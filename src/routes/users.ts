import HomeService from '../services/userService';

const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
import Logger from '../logger/logger';
import {Config} from '../config';

const TAG = 'Users service';

router.options('*', function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'PUT, POST, DELETE, GET');
    res.header('Access-Control-Allow-Headers', 'content-type,  x-access-token');
    res.send();
});

router.post('/authenticate', function(req, res, next) {
    res.header('Content-Type', 'application/json');
    res.header('Access-Control-Allow-Origin', '*');

    new HomeService().getUser(req.body.name)
        .then(data => {
            Logger.e(TAG, data);
            console.log(req.body);
            if (data && req.body.password === data.password) {
                const user = {
                    name: data.name,
                    role: data.role
                };
                const token = jwt.sign(user, Config.auth.secret, {
                    expiresIn: 86400 // expires in 24 hours
                });

                // return the information including token as JSON
                res.json({
                    success: true,
                        message: 'Enjoy token!',
                    token: token
                });
            } else {
                res.status(401);
                res.json({ success: false, message: 'Authentication failed. User not found.' });
            }
        })
        .catch(e => {
            res.status(500);
            res.send(e);
            console.log('error', e);
        });
});

router.get('/data', function (req, res, next) {
    res.header('Content-Type', 'application/json');
    res.header('Access-Control-Allow-Origin', '*');
    const token = jwt.decode(req.header('x-access-token'));
    new HomeService().getUser(token.name)
        .then(r => res.send(r.data))
        .catch(e => {
            res.status(500);
            res.send(e);
        });
});

module.exports = router;
