/**
 * Singleton service for connection to MongoDB only once
 */
const MongoClient = require('mongodb').MongoClient;
const config = require('../config');
let dbConnection;
module.exports = {};
const connectOnce = module.exports.connectOnce = callback => {
    MongoClient.connect(config.db.url, (err, client) => {
        if (err) {
            throw err;
        }
        dbConnection = client.db('home');
        callback(dbConnection);
    });
};
const reconnect = callback => {
    connectOnce(callback);
};
module.exports.getDb = () => {
    return new Promise((resolve, reject) => {
        if (!dbConnection) {
            reconnect(db => {
                resolve(db);
            });
        }
        else {
            resolve(dbConnection);
        }
    });
};
