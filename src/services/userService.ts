var mongoService = require('./mongoService');

export default class HomeService {

  public getUser(name: string) {
    return mongoService.getDb()
      .then(db => db.collection('homes').findOne({name}));
  }
}
