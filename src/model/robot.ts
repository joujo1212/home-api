export interface Robot {
  id?: string;
  time?: number;
  lastPing?: number;
}