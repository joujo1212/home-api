const moment = require('moment');
const chalk = require('chalk');
const log = console.log;
const fs = require('fs');

/**
 * Supported colors:
 black
 red
 green
 yellow
 blue (on Windows the bright version is used since normal blue is illegible)
 magenta
 cyan
 white
 gray
 */
const format = (tag, text, color) => {
    const time = moment().format('YYYY-M-D HH:mm:ss.SSS');
    if (typeof text === 'object') {
        text = JSON.stringify(text);
    }
    const logText = chalk[color](`[${time}] ${chalk.bold(tag)}: ${text}`);
    log(logText);
    fs.writeFile('log.txt', logText + '\n', {flag: 'a'});
};

export default class Logger {
    private static format(tag: string, text: string, color: string) {
      const time = moment().format('YYYY-M-D HH:mm:ss.SSS');
      if (typeof text === 'object') {
        text = JSON.stringify(text);
      }
      const logText = chalk[color](`[${time}] ${chalk.bold(tag)}: ${text}`);
      log(logText);
      fs.writeFile('log.txt', logText + '\n', {flag: 'a'});
    };

    public static e(tag: string, text: string): void {
        this.format(tag, text, 'red');
    }
}

// module.exports = {
//     e: function(tag, text) {
//         format(tag, text, 'red');
//     },
//     d: function(tag, text) {
//         format(tag, text, 'cyan');
//     },
//     i: function(tag, text) {
//         format(tag, text, 'white');
//     }
// };
