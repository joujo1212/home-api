"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var authMiddleware = require('../routes/middleware');
var mongoService = require('../services/mongoService');
var users = require('../routes/users');
var Rest = (function () {
    function Rest() {
        this.app = express();
        this.init();
    }
    Rest.prototype.init = function () {
        this.app.set('views', path.join(__dirname, '../views'));
        this.app.set('view engine', 'pug');
        this.app.use(logger('dev'));
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use(cookieParser());
        this.app.use(express.static(path.join(__dirname, 'public')));
        this.app.use('/users', users);
        this.app.use(authMiddleware);
        this.app.use(function (req, res, next) {
            var err = new Error('Not Found');
            err.status = 404;
            next(err);
        });
        this.app.use(function (err, req, res, next) {
            res.locals.message = err.message;
            res.locals.error = req.app.get('env') === 'development' ? err : {};
            res.status(err.status || 500);
            res.render('error');
        });
        mongoService.connectOnce(function (db) {
            console.log('DB connected to: ', db.topology.s.host);
        });
    };
    Rest.prototype.start = function () {
        this.app.listen(3000, function () {
            console.log('Home-api is running on port 3000');
        });
    };
    return Rest;
}());
exports.default = Rest;
