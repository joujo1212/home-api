"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var MongoClient = require('mongodb').MongoClient;
var config_1 = require("../config");
var dbConnection;
module.exports = {};
var connectOnce = module.exports.connectOnce = function (callback) {
    MongoClient.connect(config_1.Config.db.url, function (err, client) {
        if (err) {
            throw err;
        }
        dbConnection = client.db('home');
        callback(dbConnection);
    });
};
var reconnect = function (callback) {
    connectOnce(callback);
};
module.exports.getDb = function () {
    return new Promise(function (resolve, reject) {
        if (!dbConnection) {
            reconnect(function (db) {
                resolve(db);
            });
        }
        else {
            resolve(dbConnection);
        }
    });
};
