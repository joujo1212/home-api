"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoService = require('./mongoService');
var HomeService = (function () {
    function HomeService() {
    }
    HomeService.prototype.getUser = function (name) {
        return mongoService.getDb()
            .then(function (db) { return db.collection('homes').findOne({ name: name }); });
    };
    return HomeService;
}());
exports.default = HomeService;
