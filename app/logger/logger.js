var moment = require('moment');
var chalk = require('chalk');
var log = console.log;
var fs = require('fs');
var format = function (tag, text, color) {
    var time = moment().format('YYYY-M-D HH:mm:ss.SSS');
    if (typeof text === 'object') {
        text = JSON.stringify(text);
    }
    var logText = chalk[color]("[" + time + "] " + chalk.bold(tag) + ": " + text);
    log(logText);
    fs.writeFile('log.txt', logText + '\n', { flag: 'a' });
};
module.exports = {
    e: function (tag, text) {
        format(tag, text, 'red');
    },
    d: function (tag, text) {
        format(tag, text, 'cyan');
    },
    i: function (tag, text) {
        format(tag, text, 'white');
    }
};
