"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Message = (function () {
    function Message(from, content) {
        this.from = from;
        this.content = content;
    }
    return Message;
}());
exports.Message = Message;
