"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var jwt = require('jsonwebtoken');
var config_1 = require("../config");
module.exports = function (req, res, next) {
    console.log(req.body);
    if (req.method === 'OPTIONS') {
        next();
        return;
    }
    var token = req.query.token || req.headers['x-access-token'];
    if (token) {
        jwt.verify(token, config_1.Config.auth.secret, function (err, decoded) {
            if (err) {
                res.header('Content-Type', 'application/json');
                res.header('Access-Control-Allow-Origin', '*');
                res.status(401);
                return res.send({ success: false, message: 'Failed to authenticate token.' });
            }
            else {
                req.decoded = decoded;
                next();
            }
        });
    }
    else {
        return res.status(403).send({
            success: false,
            message: 'No token provided.'
        });
    }
};
