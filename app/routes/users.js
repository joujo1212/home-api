"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var userService_1 = require("../services/userService");
var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var config_1 = require("../config");
router.options('*', function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'PUT, POST, DELETE, GET');
    res.header('Access-Control-Allow-Headers', 'content-type,  x-access-token');
    res.send();
});
router.post('/authenticate', function (req, res, next) {
    res.header('Content-Type', 'application/json');
    res.header('Access-Control-Allow-Origin', '*');
    new userService_1.default().getUser(req.body.name)
        .then(function (data) {
        console.log(data);
        console.log(req.body);
        if (data && req.body.password === data.password) {
            var user = {
                name: data.name,
                role: data.role
            };
            var token = jwt.sign(user, config_1.Config.auth.secret, {
                expiresIn: 86400
            });
            res.json({
                success: true,
                message: 'Enjoy token!',
                token: token
            });
        }
        else {
            res.status(401);
            res.json({ success: false, message: 'Authentication failed. User not found.' });
        }
    })
        .catch(function (e) {
        res.status(500);
        res.send(e);
        console.log('error', e);
    });
});
router.get('/data', function (req, res, next) {
    res.header('Content-Type', 'application/json');
    res.header('Access-Control-Allow-Origin', '*');
    var token = jwt.decode(req.header('x-access-token'));
    new userService_1.default().getUser(token.name)
        .then(function (r) { return res.send(r.data); })
        .catch(function (e) {
        res.status(500);
        res.send(e);
    });
});
module.exports = router;
