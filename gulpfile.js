var gulp = require("gulp");
var ts = require("gulp-typescript");
var nodemon = require("gulp-nodemon");
var Cache = require('gulp-file-cache');
var cache = new Cache();

/**
 * Internal tasks
 */
// gulp.task("copy", function () {
//   return gulp.src('src/views/*.pug')
//       // .pipe(pug())
//       .pipe(gulp.dest('./dist/views'))
// });

gulp.task('compile', function () {
  var project = ts.createProject({
    "target": "ES5",
    "module": "commonjs",
    "moduleResolution": "node",
    "sourceMap": true,
    "emitDecoratorMetadata": true,
    "experimentalDecorators": true,
    "removeComments": true,
    "noImplicitAny": false
  });
  var stream = gulp.src('./src/**/*.ts') // your ES2015 code
    .pipe(cache.filter()) // remember files
    .pipe(project()) // compile new ones
    .pipe(cache.cache()) // cache them
    .pipe(gulp.dest('./dist')); // write them
  return stream; // important for gulp-nodemon to wait for completion
});

/**
 * Develop task
 */
gulp.task('watch', ['compile'], function () {
  var stream = nodemon({
    script: 'dist/index.js', // run ES5 code
    watch: 'src', // watch ES2015 code
    ext: 'ts',
    tasks: ['compile'] // compile synchronously onChange
  });

  return stream;
});

/**
 * Production task
 */
gulp.task('build', ['compile']);
